import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { User, UserTypes } from '../sign-in/User';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../services/auth.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { OnChanges, AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.scss']
})
export class MentorComponent implements OnInit, AfterViewInit, OnDestroy {

  mentor: Observable<User>;
  students: User[] = [];
  displayedColumns = ['photoURL', 'name', 'points'];
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MatTableDataSource<User> = new MatTableDataSource<User>(this.students);
  subscription: Subscription;

  constructor(private db: AngularFirestore, private _authService: AuthService, private router: Router) {
    this.mentor = _authService.user;
    this.subscription = this.mentor.subscribe(mentor => {
      this.getStudentData(mentor);
    });
  }

  ngOnInit() { }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  getStudentData(mentor) {
    this.db.collection('users', ref =>
      ref.where('type', '==', 'student')
        .where('center', '==', mentor.center)).snapshotChanges().map(action => {
          const actions = action.map(a => {
            const data = a.payload.doc.data();
            const uid = a.payload.doc.id;
            return { uid, ...data };
          });
          return actions;
        }).subscribe((students: any) => {
          this.students = students;
          this.renderTable();
        });
  }

  openStudentProfile(row) {
    if (row && row.uid) {
      this.router.navigate(['/mentor/student/', row.uid]);
    }
  }

  renderTable() {
    this.dataSource = new MatTableDataSource<User>(this.students);
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
