import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  user;
  constructor(private _authService: AuthService) {
    this.user = _authService.user;
    _authService.user.subscribe(user => {
      _authService.redirectToDefault(user);
    });
  }

  signout() {
    this._authService.signout();
  }

}
