import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { StudentComponent } from './student/student.component';
import { MentorComponent } from './mentor/mentor.component';
import { AuthGuard } from './services/auth.guard';
import { UserTypes } from './shared/UserTypes';

const routes: Routes = [
  { path: 'signin', component: SignInComponent, canActivate: [AuthGuard] },
  { path: 'student', component: StudentComponent, canActivate: [AuthGuard], data: { roles: [UserTypes.Student] } },
  { path: 'mentor', component: MentorComponent, canActivate: [AuthGuard], data: { roles: [UserTypes.Mentor] } },
  { path: 'mentor/student/:id', component: StudentComponent, canActivate: [AuthGuard], data: { roles: [UserTypes.Mentor] } },
  { path: '**', redirectTo: 'signin' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
