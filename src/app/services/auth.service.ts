import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { User, IUser } from '../sign-in/User';
import { UserTypes } from '../shared/UserTypes';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  user: Observable<IUser>;

  constructor(public _angularFireAuth: AngularFireAuth, private _angularFirestore: AngularFirestore, private router: Router) {

    this.user = this._angularFireAuth.authState.switchMap(user => {
      if (user) {
        return this._angularFirestore.doc<IUser>(`users/${user.uid}`).valueChanges();
      } else {
        // this.router.navigate(['/signin']);
        return Observable.of(null);
      }
    });
  }

  signin(user: User) {
    return this._angularFireAuth.auth.signInWithEmailAndPassword(user.email, user.password)
  }

  signout() {
    this._angularFireAuth.auth.signOut();
  }

  redirectToDefault(user): boolean {
    if (user) {
      if (user.type === UserTypes.Mentor) {
        this.router.navigate(['/mentor']);
      } if (user.type === UserTypes.Student) {
        this.router.navigate(['/student']);
      }
      return false;
    } else {
      this.router.navigate(['/signin']);
      return true;
    }
  }

}
