import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { UserTypes } from '../shared/UserTypes';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _authService: AuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const roles = next.data['roles'];
    return this._authService.user.take(1)
      .map(user => this.checkUserRole(user, roles));
  }

  checkUserRole(user, roles = []) {
    if (user) {
      return (roles.includes(user.type)) ? true : this._authService.redirectToDefault(user);
    }
    return true;
  }

}
