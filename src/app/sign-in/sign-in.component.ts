import { Component, OnInit } from '@angular/core';
import { User, UserTypes } from './User';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  user: User = new User();
  error = '';
  constructor(private db: AngularFirestore, public _angularFireAuth: AngularFireAuth, private _authService: AuthService,
    private router: Router) { }

  ngOnInit() { }

  signIn() {
    this.error = '';
    this._authService.signin(this.user)
      .catch(err => this.error = 'Invalid Username or Password');
  }
}
