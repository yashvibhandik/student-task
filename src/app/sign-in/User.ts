export interface IUser {
    uid?: string;
    email: string;
    password: string;
    displayName?: string;
    center?: string;
    photoURL?: string;
    points?: string;
    type?: string;
    type_center?: string;
}

export class User implements IUser {
    uid?: string;
    email: string;
    password: string;
    displayName?: string;
    center?: string;
    photoURL?: string;
    points?: string;
    type?: string;
    type_center?: string;
}

export const UserTypes = {
    Mentor: 'mentor',
    Student: 'student'
};
