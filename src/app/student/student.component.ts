import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { IUser, User, UserTypes } from '../sign-in/User';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {

  student: Observable<User>;

  constructor(private db: AngularFirestore, private _authService: AuthService, private _activatedRoute: ActivatedRoute,
    private router: Router) {
    const params = this._activatedRoute.snapshot.params;
    if (params && params.id) {
      this.student = this.getUserById(params.id);
    } else {
      this.student = _authService.user;
    }
  }

  ngOnInit() { }

  getUserById(uid: string): Observable<User> {
    return this.db.doc<User>(`users/${uid}`).valueChanges();
  }

}
